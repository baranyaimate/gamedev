# BtcMiner
# Ötlet:

🎮A mi játékunk egy belső nézetes bányászos játék lesz. 

⛰️A játékban található lesz egy bánya ami random generálodik, a kinti terep nem lesz nagy, de valamennyire bejárható lesz.

🏁A játék célja, hogy minnél több bitcoint bányásszunk ki és minnél több pénzt keressünk.

💻A bitcoin eladható és átváltható dollárra egy laptop segítségével ami a bánya előtt helyezkedik el **_(mivel a bányában nincs térerő ezért sajna a bányában nem eladható a bitcoin😭)_**.

📉De vigyázni kell mivel a bitcoin árfolyama ingadozik, ezért lehet nem érdemes azonnal eladni a kriptovalutát😥.

⏲️A játék időre fog menni, a játék végén kapunk egy pontszámot, ami megetkinthető lesz egy ScoreBoard-on, ezeket az adatokat egy adatbázisban fogjuk tárolni.

🖼️A lent látható képen már a játék alpha változata fut:

![Alpha változat](https://gitlab.com/baranyaimate/gamedev/-/raw/master/README%20resources/ezgif.com-gif-maker__1_.gif)

💡**Specifikációk:**
- A bánya random generálodik, ahogy a bitcoin mennyisége és elhelyezkedése is
- A karakter képes lesz sétálni, körülnézni, futni, ugrani, interakcióba lépni a laptopjával és természetesen bányászni
- A tervek szerint történetet nem tervezünk a játékhoz
- A laptopnak lesz, egy felhasználói felülete, ahol a bitcoinnal kapcsolatos tranzakciókciós ügyeinket intézhetjük
- Azt hogy mennyi ideig tart egy kör, még nem tudjuk
- A bitcoin bányászás ellen fog játszani az idő és a környezet

⌨**Fejlesztők:**
- **Hajas Erik**👱
- **Baranyai Máté**👱

# Ami változott:

Nincs bánya, bányászás helyett bitcoin érméket veszünk fel ezeknek az értékük egy random érték.

A ScoreBoard nem készült el. Kisebb bugok és teljesítménybeli ingások felfedezhetőek a játékban.

# Irányítás:

- **W** - Előre⬆️
- **S** - Hátra⬇️
- **A** - Balra⬅️
- **D** - Jobbra➡️


- **SPACE** - ugrás🦘
- **Left SHIFT** - futás (ha nyomva tartjuk)🏃
- **MOUSE** - körültekintés↕️↔️

