using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    [SerializeField]
    private Text exchangeRate;
    [SerializeField]
    private Text playerBtc;
    [SerializeField]
    private Text playerHuf;
    [SerializeField]
    private Text counter;
    [SerializeField]
    private float timeInMinutes;
    [SerializeField] 
    private GameObject endPanel;
    [SerializeField] 
    private GameObject mainUi;

    private void Start()
    {
        timeInMinutes *= 60;
        Invoke(nameof(GameIsEnded), timeInMinutes);
    }

    private void Update()
    {
        exchangeRate.text = $"1BTC: {BitcoinPrice.bitcoinPrice:C0}";
        playerBtc.text = $"Egyenleged: {Inventory.PlayerBTC:F8} BTC";
        playerHuf.text = $"Egyenleged: {Inventory.PlayerHUF:C0}";
        counter.text = CounterFormat();
    }

    private string CounterFormat()
    {
        TimeSpan timeSpan = TimeSpan.FromSeconds(timeInMinutes);
        string timeText = $"{timeSpan.Hours:D2}:{timeSpan.Minutes:D2}:{timeSpan.Seconds:D2}";
        timeInMinutes -= Time.deltaTime;
        return timeText;
    }
    
    private void GameIsEnded()
    {
        timeInMinutes = 0f;
        Time.timeScale = 0f;
        Destroy(mainUi);
        GameObject clone = Instantiate(endPanel, new Vector3(0, 0, 0), Quaternion.identity);
        try
        {
            clone.transform.Find("PointText").GetComponent<Text>().text = $"A pontszámod: {Inventory.PlayerHUF}";
            clone.transform.Find("NewGame").GetComponent<Button>().onClick.AddListener(NewGame);
        }
        catch (Exception e)
        {
            Debug.LogWarning(e);
        }
    }

    private void NewGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        Time.timeScale = 1f;
    }
}
