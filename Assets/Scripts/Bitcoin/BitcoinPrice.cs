using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class BitcoinPrice : MonoBehaviour
{
    private float nextActionTime = 0.0f;
    [SerializeField]
    private float bitcoinRefreshRate = 0.1f;

    public static int bitcoinPrice
    {
        get;
        private set;
    }

    private void Awake()
    {
        bitcoinPrice = Random.Range(200000, 80000000);
    }

    void Update () {
        if (Time.time > nextActionTime ) {
            nextActionTime += bitcoinRefreshRate;
            bitcoinPrice = Random.Range(200000, 80000000);
        }
    }
}
