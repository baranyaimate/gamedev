using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;


public class BitcoinObjectController : MonoBehaviour
{
    [SerializeField] private float bitcoinMinValue = 0.00000002f;
    [SerializeField] private float bitcoinMaxValue = 0.0001f;
    [SerializeField] private AudioClip coinSound;
    [SerializeField] private float coinSoundVolume = 0.4f;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag.Equals("Player"))
        {
            Inventory.PlayerBTC += Random.Range(bitcoinMinValue, bitcoinMaxValue);
            AudioSource.PlayClipAtPoint(coinSound, transform.position, coinSoundVolume);
            Destroy(gameObject);
        }
    }
}
