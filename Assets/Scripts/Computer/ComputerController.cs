using System;
using UnityEngine;
using UnityEngine.UI;

public class ComputerController : MonoBehaviour
{
    [SerializeField]
    private GameObject ComputerUi;

    private InputManager inputManager;
    private GameObject computerPanel;
    private bool panelIsActive;

    void Start()
    {
        panelIsActive = false;
        inputManager = InputManager.Instance;
    }

    private void OnTriggerStay(Collider col)
    {
        if (col.tag.Equals("Player"))
        {
            if (inputManager.PlayerInteraction() && !panelIsActive && FindWithTag("ComputerMenu") == 0)
            {
                inputManager.DisableMovement();

                computerPanel = Instantiate(ComputerUi, new Vector3(0, 0, 0), Quaternion.identity);
                
                RefreshData();

                computerPanel.transform.Find("Frame").transform.Find("Border")
                    .transform.Find("SellButton").GetComponent<Button>().onClick.AddListener(SellBtc);
                
                computerPanel.transform.Find("Frame").transform.Find("Border")
                    .transform.Find("SellAllButton").GetComponent<Button>().onClick.AddListener(SellAllBtc);

                computerPanel.transform.GetChild(0).transform.localScale = new Vector3(0, 0, 0);
                
                LeanTween.scale(computerPanel.transform.GetChild(0).gameObject, 
                    new Vector3(1, 1, 1), 0.3f).setOnComplete(SetActive);
            }
            else if (inputManager.PlayerInteraction()  && panelIsActive && FindWithTag("ComputerMenu") > 0)
            {
                inputManager.OnEnable();
                
                LeanTween.scale(computerPanel.transform.GetChild(0).gameObject,
                    new Vector3(0, 0, 0), 0.3f).setOnComplete(SetDisabled);
            }
            else if(FindWithTag("ComputerMenu") > 0)
            {
                RefreshData();
            }
        }
    }

    private void RefreshData()
    {
        computerPanel.transform.Find("Frame").transform.Find("Border").transform.Find("BitcoinValue").
            GetComponent<Text>().text = $"1BTC = {BitcoinPrice.bitcoinPrice:C0}";
        
        computerPanel.transform.Find("Frame").transform.Find("Border").transform.Find("PlayerBitcoin").
            GetComponent<Text>().text = $"{Inventory.PlayerBTC:F8} BTC";
        
        computerPanel.transform.Find("Frame").transform.Find("Border").transform.Find("PlayerHUF").
            GetComponent<Text>().text = $"{Inventory.PlayerHUF:C0}";
    }

    private void SellBtc()
    {
        try
        {
            float inputBtc = float.Parse(computerPanel.transform.Find("Frame").transform.Find("Border")
                .transform.Find("SellInput").transform.Find("Text").GetComponent<Text>().text);
            
            if (inputBtc <= Inventory.PlayerBTC)
            {
                Inventory.PlayerBTC -= inputBtc;
                Inventory.PlayerHUF += (int) (float.Parse(BitcoinPrice.bitcoinPrice.ToString()) * inputBtc);
            }
        }
        catch (Exception e)
        {
            Debug.Log("Érvénytelen szám formátum!");
            Debug.LogWarning(e.Message);
        }
    }
    
    private void SellAllBtc()
    {
        Inventory.PlayerHUF += (int) (float.Parse(BitcoinPrice.bitcoinPrice.ToString()) * Inventory.PlayerBTC);
        Inventory.PlayerBTC = 0.0f;
    }

    private void SetActive()
    {
        panelIsActive = true;
        Cursor.visible = true;
    }

    private void SetDisabled()
    {
        panelIsActive = false;
        Cursor.visible = false;
        Destroy(computerPanel);
    }

    private int FindWithTag(String tag)
    {
        return GameObject.FindGameObjectsWithTag(tag).Length;
    }
}