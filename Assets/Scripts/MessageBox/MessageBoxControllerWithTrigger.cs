using System;
using UnityEngine;
using UnityEngine.UI;

public class MessageBoxControllerWithTrigger : MonoBehaviour
{

    [SerializeField] 
    private GameObject messageBox;
    [SerializeField] 
    private String message;
    
    private GameObject clone;

    /// <summary>
    /// If the player go in the trigger area,
    /// this method make a message box
    /// <para/>The method create a clone from prefab
    /// </summary>
    /// <param name="col">Trigger collider</param>
    private void OnTriggerEnter(Collider col)
    {
        if (col.tag.Equals("Player") && GameObject.FindGameObjectsWithTag("Message").Length == 0)
        {
            clone = Instantiate(messageBox, new Vector3(0, 0, 0), Quaternion.identity);
            clone.transform.GetChild(1).GetComponent<Text>().text = message;
        }
    }

    /// <summary>
    /// If the player leave the trigger area,
    /// <para/>this method call <seealso cref="TextHide"/> method
    /// </summary>
    /// <param name="col">Trigger collider</param>
    private void OnTriggerExit(Collider col)
    {
        if (col.tag.Equals("Player") && GameObject.FindGameObjectsWithTag("Message").Length > 0)
        {
            TextHide();
        }
    }

    /// <summary>
    /// Set Text size to zero with animation
    /// </summary>
    private void TextHide()
    {
        LeanTween.scale(clone.transform.GetChild(1).gameObject, new Vector3(0, 0, 0), 0.1f).setDelay(0.1f).setOnComplete(BoxHide);
    }
    
    /// <summary>
    /// Set TextBox size to zero with animation
    /// </summary>
    private void BoxHide()
    {
        LeanTween.scale(clone.transform.GetChild(0).gameObject, new Vector3(0, 0, 0), 0.1f).setDelay(0.1f).setOnComplete(DestroyClone);
    }

    
    /// <summary>
    /// Destroy gameObject
    /// </summary>
    private void DestroyClone()
    {
        Destroy(clone);
    }
}