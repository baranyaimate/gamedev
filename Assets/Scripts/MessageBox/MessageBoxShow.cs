using UnityEngine;

public class MessageBoxShow : MonoBehaviour
{
    [SerializeField] 
    private GameObject messageBox;
    [SerializeField] 
    private GameObject messageText;
    
    /// <summary>
    /// Set message box and message text size to zero, then
    /// set the normal size with <seealso cref="addText"/> method
    /// </summary>
    void Start()
    {
        messageBox.transform.localScale = new Vector3(0, 0, 0);
        messageText.transform.localScale = new Vector3(0, 0, 0);
        LeanTween.scale(messageBox, new Vector3(1, 1, 1), 0.3f).setOnComplete(addText);
    }

    
    private void addText()
    {
        LeanTween.scale(messageText, new Vector3(1, 1, 1), 0.1f);
    }
        
}