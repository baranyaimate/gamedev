using UnityEngine;

public class InputManager : MonoBehaviour
{
    private InputSystem playerControls;
    private static InputManager _instance;

    public static InputManager Instance
    {
        get => _instance;
    }

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
        playerControls = new InputSystem();
        Cursor.visible = false;
    }

    /// <summary>
    /// Enable all available controls
    /// </summary>
    public void OnEnable()
    {
        playerControls.Enable();
    }

    /// <summary>
    /// Disable all available controls
    /// </summary>
    private void OnDisable()
    {
        playerControls.Disable();
    }

    /// <summary>
    /// Get Player Movement
    /// </summary>
    /// <returns>Vector2</returns>
    public Vector2 GetPlayerMovement()
    {
        return playerControls.Player.Movement.ReadValue<Vector2>();
    }

    /// <summary>
    /// Get Camera Delta rotation
    /// </summary>
    /// <returns>Vector2</returns>
    public Vector2 GetMouseDelta()
    {
        return playerControls.Player.Look.ReadValue<Vector2>();
    }
    
    /// <summary>
    /// Player Jump detection:
    /// <para/>true: if player press the jump button
    /// <para/>false: if player doesn't press the jump button
    /// </summary>
    /// <returns>bool</returns>
    public bool PlayerIsJumped()
    {
        return playerControls.Player.Jump.triggered;
    }

    /// <summary>
    /// Player Jump detection:
    /// <para/>true: if player hold the run button
    /// <para/>false: if player doesn't hold the run button
    /// </summary>
    /// <returns></returns>
    public bool PlayerIsRunning()
    {
        return playerControls.Player.Run.ReadValue<float>() > 0;
    }

    /// <summary>
    /// Player Interaction detection:
    /// <para/>true: if player press the interaction button
    /// <para/>false: if player doesn't press the interaction button
    /// </summary>
    /// <returns>bool</returns>
    public bool PlayerInteraction()
    {
        return playerControls.Player.Interact.triggered;
    }

    /// <summary>
    /// Disable Player Movement:
    /// <list type="bullet">
    /// <item>
    /// <description>Disable Movement</description>
    /// </item>
    /// <item>
    /// <description>Disable Camera Rotation</description>
    /// </item>
    /// <item>
    /// <description>Disable Disable Running</description>
    /// </item>
    /// <item>
    /// <description>Disable Jumping</description>
    /// </item>
    /// </list>
    /// But the Interaction button still available
    /// </summary>
    public void DisableMovement()
    {
        playerControls.Player.Run.Disable();
        playerControls.Player.Look.Disable();
        playerControls.Player.Movement.Disable();
        playerControls.Player.Jump.Disable();
    }
}
